# Builder

Imaginons qu'on a une interface qui retourne une liste des items (mobiles) en format JSON pour nos clients. \
Un nouveau client qui s'ajoute mais il veut la liste en format XML.

## Solution 
Creer un adaptateur qui permet de retourner la liste souhaité en format XML sans modifier le comportement de notre code 