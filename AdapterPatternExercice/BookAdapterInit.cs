using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Adapter.Test;

public class BookAdapter
{
    

    public class Enonce
    {
        // [Fact]
        // public void Q01_Creer_une_struct_Book()
        // {
        //     Book book = new Book(0, "FirstBook");
        //
        //     Assert.Equal(0, book.Id);
        //     Assert.Equal("FirstBook", book.Name);
        // }

        /*[Fact]
        public void Q01_Creer_une_classe_IdOrder_avec_une_methode_OrderById()
        {
            IList<Book> books = new List<Book>()
            {
                new Book(2,"A"),
                new Book(0,"C"),
                new Book(1,"B"),
            };

            IdOrder order = new IdOrder();
            var result = order.OrderById(books);

            IList<Book> expectedBooks = books.OrderBy(x => x.Id).ToList();

            Assert.Equal(expectedBooks, result);
        }*/

        /*[Fact]
        public void Q02_Creer_une_classe_NameOrder_avec_une_methode_OrderByName()
        {
            IList<Book> books = new List<Book>()
            {
                new Book(2,"A"),
                new Book(0,"C"),
                new Book(1,"B"),
            };

            NameOrder order = new NameOrder();
            var result = order.OrderByName(books);

            IList<Book> expectedBooks = books.OrderBy(x => x.Name).ToList();

            Assert.Equal(expectedBooks, result);
        }*/

        /*[Fact]
        public void Q03_Creer_une_classe_IdSorter_avec_une_methode_Sort_utilisant_IdOrder()
        {
            IList<Book> books = new List<Book>()
            {
                new Book(2,"A"),
                new Book(0,"C"),
                new Book(1,"B"),
            };

            IdSorter order = new IdSorter();
            var result = order.Sort(books);

            IList<Book> expectedBooks = new IdOrder().OrderById(books);

            Assert.Equal(expectedBooks, result);
        }*/

        /*[Fact]
        public void Q04_Creer_une_classe_NameSorter_avec_une_methode_Sort_utilisant_NameOrder()
        {
            IList<Book> books = new List<Book>()
            {
                new Book(2,"A"),
                new Book(0,"C"),
                new Book(1,"B"),
            };

            NameSorter order = new NameSorter();
            var result = order.Sort(books);

            IList<Book> expectedBooks = new NameOrder().OrderByName(books);

            Assert.Equal(expectedBooks, result);
        }*/

        /*[Fact]
        public void Q05_Unifier_IdSorter_et_NameSorter_avec_une_interface_Sorter()
        {
            IList<Book> books = new List<Book>()
            {
                new Book(2,"A"),
                new Book(0,"C"),
                new Book(1,"B"),
            };

            ISorter nameSorter = new NameSorter();
            NameOrder nameOrder = new NameOrder();
            Assert.Equal(nameOrder.OrderByName(books), nameSorter.Sort(books));

            ISorter idSorter = new IdSorter();
            IdOrder idOrder = new IdOrder();
            Assert.Equal(idOrder.OrderById(books), idSorter.Sort(books));
        }*/

    }
}