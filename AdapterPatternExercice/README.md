# Exercice : Adapter

## Objectifs

- Savoir construire un adapter

**Temps indicatif** : 30 minutes

## Etapes

Chaque étape est matérialisée par un test unitaire dans la classe ```Enonce``` du fichier ```BookAdapterInit.cs```.

Pour chaque etape :
- Décommenter le test
- Ecrire le code minimal permettant de faire compiler le programme
- Ecrire le code minimal pour faire passer le test au vert
- Passer à l'étape (test) suivante